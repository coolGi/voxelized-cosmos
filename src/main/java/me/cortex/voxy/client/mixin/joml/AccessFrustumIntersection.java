package me.cortex.voxy.client.mixin.joml;

import com.badlogic.gdx.math.Vector4;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(value = FrustumIntersection.class, remap = false)
public interface AccessFrustumIntersection {
    @Accessor Vector4[] getPlanes();
}
