package me.cortex.voxy.client.config;

import org.lwjgl.opengl.GL;
import org.quiltmc.config.api.ReflectiveConfig;
import org.quiltmc.config.api.values.TrackedValue;
import org.quiltmc.loader.api.config.v2.QuiltConfig;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;

public class VoxyConfig extends ReflectiveConfig {
    public static final VoxyConfig INSTANCE = QuiltConfig.create("", "voxy", VoxyConfig.class);

    public TrackedValue<Boolean> enabled = this.value(true);
    public TrackedValue<Boolean> ingestEnabled = this.value(true);
    public TrackedValue<Integer> qualityScale = this.value(12);
    public TrackedValue<Integer> maxSections = this.value(200_000);
    public TrackedValue<Integer> renderDistance = this.value(128);
    public TrackedValue<Integer> geometryBufferSize = this.value((1<<30)/8);
    public TrackedValue<Integer> ingestThreads = this.value(2);
    public TrackedValue<Integer> savingThreads = this.value(4);
    public TrackedValue<Integer> renderThreads = this.value(5);
    public TrackedValue<Boolean> useMeshShaderIfPossible = this.value(true);


    public boolean useMeshShaders() {
        var cap = GL.getCapabilities();
        return this.useMeshShaderIfPossible.getRealValue() && cap.GL_NV_mesh_shader && cap.GL_NV_representative_fragment_test;
    }
}
