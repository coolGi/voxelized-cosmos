package me.cortex.voxy.client.core;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Frustum;
import com.badlogic.gdx.math.Matrix4;
import finalforeach.cosmicreach.gamestates.InGame;
import finalforeach.cosmicreach.settings.GraphicsSettings;
import finalforeach.cosmicreach.world.World;
import finalforeach.cosmicreach.world.chunks.Chunk;
import me.cortex.voxy.client.Voxy;
import me.cortex.voxy.client.config.VoxyConfig;
import me.cortex.voxy.client.core.rendering.*;
import me.cortex.voxy.client.core.rendering.building.RenderGenerationService;
import me.cortex.voxy.client.core.rendering.post.PostProcessing;
import me.cortex.voxy.client.core.util.DebugUtil;
import me.cortex.voxy.client.core.util.IrisUtil;
import me.cortex.voxy.client.saver.ContextSelectionSystem;
import me.cortex.voxy.common.world.WorldEngine;
import me.cortex.voxy.client.importers.WorldImporter;
import org.lwjgl.opengl.GL11;

import java.io.File;
import java.util.*;

import static org.lwjgl.opengl.GL30C.GL_DRAW_FRAMEBUFFER_BINDING;
import static org.lwjgl.opengl.GL30C.GL_FRAMEBUFFER;

//Core class that ingests new data from sources and updates the required systems

//3 primary services:
// ingest service: this takes in unloaded chunk events from the client, processes the chunk and critically also updates the lod view of the world
// render data builder service: this service builds the render data from build requests it also handles the collecting of build data for the selected region (only axis aligned single lod tasks)
// serialization service: serializes changed world data and ensures that the database and any loaded data are in sync such that the database can never be more updated than loaded data, also performs compression on serialization

//there are multiple subsystems
//player tracker system (determines what lods are loaded and used by the player)
//updating system (triggers render data rebuilds when something from the ingest service causes an LOD change)
//the render system simply renders what data it has, its responsable for gpu memory layouts in arenas and rendering in an optimal way, it makes no requests back to any of the other systems or services, it just applies render data updates

//There is strict forward only dataflow
//Ingest -> world engine -> raw render data -> render data
public class VoxelCore {
    private final WorldEngine world;
    private final DistanceTracker distanceTracker;
    private final RenderGenerationService renderGen;
    private final RenderTracker renderTracker;

    private final AbstractFarWorldRenderer renderer;
    private final ViewportSelector viewportSelector;
    private final PostProcessing postProcessing;

    //private final Thread shutdownThread = new Thread(this::shutdown);

    private WorldImporter importer;
    public VoxelCore(ContextSelectionSystem.Selection worldSelection) {
        this.world = worldSelection.createEngine();
        var cfg = worldSelection.getConfig();
        System.out.println("Initializing voxy core");

        //Trigger the shared index buffer loading
        SharedIndexBuffer.INSTANCE.id();
        if (VoxyConfig.CONFIG.useMeshShaders()) {
            this.renderer = new NvMeshFarWorldRenderer(VoxyConfig.CONFIG.geometryBufferSize, VoxyConfig.CONFIG.maxSections);
            System.out.println("Using NvMeshFarWorldRenderer");
        } else {
            this.renderer = new Gl46FarWorldRenderer(VoxyConfig.CONFIG.geometryBufferSize, VoxyConfig.CONFIG.maxSections);
            System.out.println("Using Gl46FarWorldRenderer");
        }
        this.viewportSelector = new ViewportSelector<>(this.renderer::createViewport);
        System.out.println("Renderer initialized");

        this.renderTracker = new RenderTracker(this.world, this.renderer);
        this.renderGen = new RenderGenerationService(this.world, this.renderer.getModelManager(), VoxyConfig.CONFIG.renderThreads, this.renderTracker::processBuildResult);
        this.world.setDirtyCallback(this.renderTracker::sectionUpdated);
        this.renderTracker.setRenderGen(this.renderGen);
        System.out.println("Render tracker and generator initialized");

        //To get to chunk scale multiply the scale by 2, the scale is after how many chunks does the lods halve
        int q = VoxyConfig.INSTANCE.qualityScale.getRealValue();
        // FIXME[CR]: These are hardcoded atm
        int minY = Voxy.WORLD_BASE/2;
        int maxY = Voxy.WORLD_TOP/2;

        if (cfg.minYOverride != Integer.MAX_VALUE) {
            minY = cfg.minYOverride;
        }

        if (cfg.maxYOverride != Integer.MIN_VALUE) {
            maxY = cfg.maxYOverride;
        }

        this.distanceTracker = new DistanceTracker(this.renderTracker, new int[]{q,q,q,q},
                (VoxyConfig.INSTANCE.renderDistance.getRealValue()<0?VoxyConfig.INSTANCE.renderDistance.getRealValue():((VoxyConfig.INSTANCE.renderDistance.getRealValue()+1)/2)),
                3, minY, maxY);
        System.out.println("Distance tracker initialized");

        this.postProcessing = new PostProcessing();

        this.world.getMapper().setCallbacks(this.renderer::addBlockState, this.renderer::addBiome);


        ////Resave the db incase it failed a recovery
        //this.world.getMapper().forceResaveStates();

        var biomeRegistry = MinecraftClient.getInstance().world.getRegistryManager().get(RegistryKeys.BIOME);
        for (var biome : this.world.getMapper().getBiomeEntries()) {
            //this.renderer.getModelManager().addBiome(biome.id, biomeRegistry.get(new Identifier(biome.biome)));
            this.renderer.addBiome(biome);
        }

        for (var state : this.world.getMapper().getStateEntries()) {
            //this.renderer.getModelManager().addEntry(state.id, state.state);
            this.renderer.addBlockState(state);
        }
        //this.renderer.getModelManager().updateEntry(0, Blocks.GRASS_BLOCK.getDefaultState());

        System.out.println("Voxy core initialized");
    }



    public void enqueueIngest(Chunk worldChunk) {
        this.world.ingestService.enqueueIngest(worldChunk);
    }

    boolean firstTime = true;
    public void renderSetup(Frustum frustum, Camera camera) {
        if (this.firstTime) {
            this.distanceTracker.init((int) camera.position.x, (int) camera.position.z);
            this.firstTime = false;
            //this.renderTracker.addLvl0(0,6,0);
        }
        this.distanceTracker.setCenter((int) camera.position.x, (int) camera.position.y, (int) camera.position.z);
        this.renderer.setupRender(frustum, camera);
    }

    private static Matrix4 makeProjectionMatrix(float near, float far) {
        //TODO: use the existing projection matrix use mulLocal by the inverse of the projection and then mulLocal our projection

        var projection = new Matrix4();
        var client = InGame.IN_GAME;

        float fov = GraphicsSettings.fieldOfView.getValue();

        projection.setToProjection(fov * 0.01745329238474369f,
                client.getWorldCamera().viewportWidth / client.getWorldCamera().viewportWidth,
                near, far);
        return projection;
    }

    private static Matrix4 computeProjectionMat() {
        return InGame.IN_GAME.getWorldCamera().combined.cpy().mulLeft(
                makeProjectionMatrix(0.05f, InGame.IN_GAME.getWorldCamera().far).inv()
        ).mulLeft(makeProjectionMatrix(16, 16*3000));
    }

    public void renderOpaque(MatrixStack matrices, double cameraX, double cameraY, double cameraZ) {
        if (IrisUtil.irisShadowActive()) {
            return;
        }
        matrices.push();
        matrices.translate(-cameraX, -cameraY, -cameraZ);
        DebugUtil.setPositionMatrix(matrices);
        matrices.pop();
        //this.renderer.getModelManager().updateEntry(0, Blocks.DIRT_PATH.getDefaultState());

        //this.renderer.getModelManager().updateEntry(0, Blocks.COMPARATOR.getDefaultState());
        //this.renderer.getModelManager().updateEntry(0, Blocks.OAK_LEAVES.getDefaultState());

        //var fb = Iris.getPipelineManager().getPipelineNullable().getSodiumTerrainPipeline().getTerrainSolidFramebuffer();
        //fb.bind();

        var projection = computeProjectionMat();
        //var projection = RenderSystem.getProjectionMatrix();//computeProjectionMat();
        var viewport = this.viewportSelector.getViewport();
        viewport.setProjection(projection).setModelView(matrices.peek().getPositionMatrix()).setCamera(cameraX, cameraY, cameraZ);

        int boundFB = GL11.glGetInteger(GL_DRAW_FRAMEBUFFER_BINDING);
        this.postProcessing.setup(MinecraftClient.getInstance().getFramebuffer().textureWidth, MinecraftClient.getInstance().getFramebuffer().textureHeight, boundFB);

        this.renderer.renderFarAwayOpaque(viewport);

        //Compute the SSAO of the rendered terrain
        this.postProcessing.computeSSAO(projection, matrices);

        //We can render the translucent directly after as it is the furthest translucent objects
        this.renderer.renderFarAwayTranslucent(viewport);


        this.postProcessing.renderPost(projection, RenderSystem.getProjectionMatrix(), boundFB);

    }

    public void addDebugInfo(List<String> debug) {
        debug.add("");
        debug.add("");
        debug.add("Voxy Core: " + Voxy.VERSION);
        debug.add("Ingest service tasks: " + this.world.ingestService.getTaskCount());
        debug.add("Saving service tasks: " + this.world.savingService.getTaskCount());
        debug.add("Render service tasks: " + this.renderGen.getTaskCount());
        debug.add("Loaded cache sizes: " + Arrays.toString(this.world.getLoadedSectionCacheSizes()));
        debug.add("Mesh cache count: " + this.renderGen.getMeshCacheCount());
        this.renderer.addDebugData(debug);
    }

    //Note: when doing translucent rendering, only need to sort when generating the geometry, or when crossing into the center zone
    // cause in 99.99% of cases the sections dont need to be sorted
    // since they are AABBS crossing the normal is impossible without one of the axis being equal

    public void shutdown() {
        //if (Thread.currentThread() != this.shutdownThread) {
        //    Runtime.getRuntime().removeShutdownHook(this.shutdownThread);
        //}

        //this.world.getMapper().forceResaveStates();
        if (this.importer != null) {
            System.out.println("Shutting down importer");
            try {this.importer.shutdown();this.importer = null;} catch (Exception e) {System.err.println(e);}
        }
        System.out.println("Shutting down voxel core");
        try {this.renderGen.shutdown();} catch (Exception e) {System.err.println(e);}
        System.out.println("Render gen shut down");
        try {this.world.shutdown();} catch (Exception e) {System.err.println(e);}
        System.out.println("World engine shut down");
        try {this.renderer.shutdown(); this.viewportSelector.free();} catch (Exception e) {System.err.println(e);}
        System.out.println("Renderer shut down");
        if (this.postProcessing!=null){try {this.postProcessing.shutdown();} catch (Exception e) {System.err.println(e);}}
        System.out.println("Voxel core shut down");
    }

    public boolean createWorldImporter(World mcWorld, File worldPath) {
        if (this.importer != null) {
            return false;
        }
        var importer = new WorldImporter(this.world, mcWorld);
        var bossBar = new ClientBossBar(MathHelper.randomUuid(), Text.of("Voxy world importer"), 0.0f, BossBar.Color.GREEN, BossBar.Style.PROGRESS, false, false, false);
        MinecraftClient.getInstance().inGameHud.getBossBarHud().bossBars.put(bossBar.getUuid(), bossBar);
        importer.importWorldAsyncStart(worldPath, 4, (a,b)->
                MinecraftClient.getInstance().executeSync(()-> {
                    bossBar.setPercent(((float) a)/((float) b));
                    bossBar.setName(Text.of("Voxy import: "+ a+"/"+b + " region files"));
                }),
                ()-> {
                    MinecraftClient.getInstance().executeSync(()-> {
                        MinecraftClient.getInstance().inGameHud.getBossBarHud().bossBars.remove(bossBar.getUuid());
                        String msg = "Voxy world import finished";
                        MinecraftClient.getInstance().inGameHud.getChatHud().addMessage(Text.literal(msg));
                        System.err.println(msg);
                    });
                    this.importer = null;
                });
        this.importer = importer;
        return true;
    }

    public WorldEngine getWorldEngine() {
        return this.world;
    }
}
