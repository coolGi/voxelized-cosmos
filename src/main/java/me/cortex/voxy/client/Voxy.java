package me.cortex.voxy.client;

import finalforeach.cosmicreach.world.World;
import me.cortex.voxy.client.core.VoxelCore;
import me.cortex.voxy.client.saver.ContextSelectionSystem;
import org.coolcosmos.cosmicquilt.api.entrypoint.client.ClientModInitializer;
import org.quiltmc.loader.api.ModContainer;

public class Voxy implements ClientModInitializer {
    public static String VERSION;

    private static final ContextSelectionSystem selector = new ContextSelectionSystem();

    // TODO[CR]: Dont hardcode these
    public static final int WORLD_BASE = 0;
    public static final int  WORLD_TOP = 255;

    public static VoxelCore createVoxelCore(World world) {
        var selection = selector.getBestSelectionOrCreate(world);
        return new VoxelCore(selection);
    }

    @Override
    public void onInitializeClient(ModContainer mod) {
        VERSION = mod.metadata().version().raw();

//        ClientCommandRegistrationCallback.EVENT.register((dispatcher, registryAccess) -> {
//            dispatcher.register(WorldImportCommand.register());
//        });
    }
}
