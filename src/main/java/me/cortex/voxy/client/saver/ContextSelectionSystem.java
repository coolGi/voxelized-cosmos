package me.cortex.voxy.client.saver;

import finalforeach.cosmicreach.world.World;
import me.cortex.voxy.client.config.VoxyConfig;
import me.cortex.voxy.common.storage.StorageBackend;
import me.cortex.voxy.common.storage.compressors.ZSTDCompressor;
import me.cortex.voxy.common.storage.config.ConfigBuildCtx;
import me.cortex.voxy.common.storage.config.StorageConfig;
import me.cortex.voxy.common.storage.other.CompressionStorageAdaptor;
import me.cortex.voxy.common.storage.rocksdb.RocksDBStorageBackend;
import me.cortex.voxy.common.world.WorldEngine;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

//Sets up a world engine with respect to the world the client is currently loaded into
// this is a bit tricky as each world has its own config, e.g. storage configuration
public class ContextSelectionSystem {
    public static class WorldConfig {
        public int minYOverride = Integer.MAX_VALUE;
        public int maxYOverride = Integer.MIN_VALUE;
        public StorageConfig storageConfig;
    }

    public static class Selection {
        private final Path selectionFolder;
        private final String worldId;

        private WorldConfig config;

        public Selection(Path selectionFolder, String worldId) {
            this.selectionFolder = selectionFolder;
            this.worldId = worldId;
            loadStorageConfigOrDefault();
        }

        private void loadStorageConfigOrDefault() {
            this.config = new WorldConfig();

            //Load the default config
            var baseDB = new RocksDBStorageBackend.Config();

            var compressor = new ZSTDCompressor.Config();
            compressor.compressionLevel = 7;

            var compression = new CompressionStorageAdaptor.Config();
            compression.delegate = baseDB;
            compression.compressor = compressor;

            this.config.storageConfig = compression;

            this.save();
        }

        public StorageBackend createStorageBackend() {
            var ctx = new ConfigBuildCtx();
            ctx.setProperty(ConfigBuildCtx.BASE_SAVE_PATH, this.selectionFolder.toString());
            ctx.setProperty(ConfigBuildCtx.WORLD_IDENTIFIER, this.worldId);
            ctx.pushPath(ConfigBuildCtx.DEFAULT_STORAGE_PATH);
            return this.config.storageConfig.build(ctx);
        }

        public WorldEngine createEngine() {
            return new WorldEngine(this.createStorageBackend(), VoxyConfig.INSTANCE.ingestThreads.getRealValue(), VoxyConfig.INSTANCE.savingThreads.getRealValue(), 5);
        }

        //Saves the config for the world selection or something, need to figure out how to make it work with dimensional configs maybe?
        // or just have per world config, cause when creating the world engine doing the string substitution would
        // make it automatically select the right id
        public void save() {
            VoxyConfig.INSTANCE.save();
        }

        public WorldConfig getConfig() {
            return this.config;
        }
    }

    //Gets dimension independent base world, if singleplayer, its the world name, if multiplayer, its the server ip
    private static Path getBasePath(World world) {
        return new File(world.getFullSaveFolder()).toPath();
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (byte b : hash) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    private static String getWorldId(World world) {
        String data = world.getDefaultZone().worldGenerator.seed + world.getDefaultZone().worldGenerator.getSaveKey();
        try {
            return bytesToHex(MessageDigest.getInstance("SHA-256").digest(data.getBytes())).substring(0, 32);
        } catch (
                NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    //The way this works is saves are segmented into base worlds, e.g. server ip, local save etc
    // these are then segmented into subsaves for different worlds within the parent
    public ContextSelectionSystem() {
    }


    public Selection getBestSelectionOrCreate(World world) {
        var path = getBasePath(world);
        try {
            Files.createDirectories(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return new Selection(path, getWorldId(world));
    }
}
